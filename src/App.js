import React from 'react';
import './App.css';
import MainNavi from './Component/Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Pricing from './Pages/Pricing';
import Services from './Pages/Services';

import Home from './Pages/Home';
import Sidebar from './Component/Sidebar';

function App() {
  return (
    <div>
      <Router>
      <MainNavi />

        <br />
        <Switch>
        <Route path='/pricing'> <Pricing /> </Route>
        <Route path = '/' exact component = {Home} />
        <Route path = "/services" > <Services /> </Route>
        </Switch>
        <Sidebar />

      </Router>
     
    </div>
  );
}

export default App;
