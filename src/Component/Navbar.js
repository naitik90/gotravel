import React, {Component}  from 'react';

import  {Nav,Navbar,Form,Button,FormControl} from 'react-bootstrap';
import { LinkContainer } from "react-router-bootstrap";
function MainNavi(){
      return(
        <div>
  <Navbar bg="dark" variant="dark" sticky="top">
  <LinkContainer to = '/'><Navbar.Brand >GoTour</Navbar.Brand></LinkContainer>
    <Nav className="mr-auto">
    <LinkContainer to = '/'>
      <Nav.Link>Home</Nav.Link>
        </LinkContainer>
        <LinkContainer to = "/services">
        <Nav.Link>Features</Nav.Link>
        </LinkContainer>
      
      <LinkContainer to = '/pricing'>
      <Nav.Link  >Pricing</Nav.Link>

      </LinkContainer>
    </Nav>
    <Form inline>
      <FormControl type="text" placeholder="Search" className="mr-sm-2" />
      <Button variant="outline-info">Search</Button>
    </Form>
  </Navbar>
  
</div>
      )
    }
export default MainNavi;
